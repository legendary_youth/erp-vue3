import request from "@/utils/requests";
// 用户管理
export const queryDictionaryByPath = (data) => {
    return request({
        url: "/api/erpDictionary/queryDictionaryByPath",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}