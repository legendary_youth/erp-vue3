import request from "@/utils/requests";
// 用户管理
export const queryAllById = (data) => {
    return request({
        url: "/api/erpCustom/queryAllById",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const queryAll = (data) => {
    return request({
        url: "/api/erpCustom/queryAll",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const addCustom = (data) => {
    return request({
        url: "/api/erpCustom/addCustom",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const deleteById = (id) => {
    return request({
        url: "/api/erpCustom/deleteById/" + id,
        method: "get",
        // date: date,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}


export const queryCountCustom = (isQuery) => {
    return request({
        url: "/api/erpCustom/queryCountCustom/" + isQuery,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const queryNameCustomer = (uid) => {
    return request({
        url: "/api/erpCustom/queryName/" + uid,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const queryCountMonth = (isQuery) => {
    return request({
        url: "/api/erpCustom/queryCountMonth/" + isQuery,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const queryCountType = (isQuery) => {
    return request({
        url: "/api/erpCustom/queryCountType/" + isQuery,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const queryCountTypeSource = (isQuery) => {
    return request({
        url: "/api/erpCustom/queryCountTypeSource/" + isQuery,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}
