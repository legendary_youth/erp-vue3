import request from "@/utils/requests";

export const queryByUId = (uid) => {
    return request({
        url: "/api/ErpDepart/queryByUId/" + uid,
        method: "get",
        // date: date,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}