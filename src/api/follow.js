import request from "@/utils/requests";
// 用户管理
export const queryFollowList = (data) => {
    return request({
        url: "/api/erpDictionary/queryFollowList",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}
export const queryFollow = (data) => {
    return request({
        url: "/api/erpDictionary/queryFollow",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}
export const queryCountMonth = (isQuery) => {
    return request({
        url: "/api/erpDictionary/queryCountMonth/" + isQuery,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}


export const addFollow = (data) => {
    return request({
        url: "/api/erpDictionary/addFollow",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const deleteFollow = (id) => {
    return request({
        url: "/api/erpDictionary/deleteFollow/" + id,
        method: "get",
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}


