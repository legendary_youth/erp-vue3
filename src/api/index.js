import request from "@/utils/requests";
// 用户管理
export const queryAll = (data) => {
    return request({
        url: "/api/erpUserInfo/queryAll",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const login = (data) => {
    return request({
        url: "/api/erpUserInfo/login",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}

export const queryName = (data) => {
    return request({
        url: "/api/erpUserInfo/queryName",
        method: "post",
        data: data,
        config: {
            headers: {
                "Content-Type": "application/json"
            },
            timeout: 10000
        }
    })
}