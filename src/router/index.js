import { createRouter, createWebHistory } from 'vue-router'
import login from '../views/components/login/login.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/homePage',
      name: 'HomePage',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Homepage.vue'),
      //定向
      redirect: '/customerAnalysis',
      // 二级三级路由
      children: [{
        path: '/customerAnalysis',
        name: 'customerAnalysis',
        component: () => import(/*webpackChunkName:'user'*/'../views/components/custom/customerAnalysis.vue'),
      }, {
        path: '/personalBriefing',
        name: 'personalBriefing',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/custom/personalBriefing.vue'),
      }, {
        path: '/customerSummary',
        name: 'customerSummary',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/custom/customerSummary.vue'),
      }, {
        path: '/followRecords',
        name: 'followRecords',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/custom/followRecords.vue'),
      }, {
        path: '/customerInformation',
        name: 'customerInformation',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/custom/customerInformation.vue'),
      }, {
        path: '/visitRecords',
        name: 'visitRecords',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/custom/visitRecords.vue'),
      }, {
        path: '/businessFlow',
        name: 'businessFlow',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/sales/businessFlow.vue'),
      }, {
        path: '/deliveryQuality',
        name: 'deliveryQuality',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/sales/deliveryQuality.vue'),
      }, {
        path: '/salesAnalysis',
        name: 'salesAnalysis',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/sales/salesAnalysis.vue'),
      }, {
        path: '/salesContract',
        name: 'salesContract',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/sales/salesContract.vue'),
      }, {
        path: '/salesReturns',
        name: 'salesReturns',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/sales/salesReturns.vue'),
      }, {
        path: '/salesShipment',
        name: 'salesShipment',
        component: () => import(/*webpackChunkName:'admin'*/'../views/components/sales/salesShipment.vue'),
        // }, {
        //   path: '/dataSource',
        //   name: 'dataSource',
        //   component: () => import(/*webpackChunkName:'admin'*/'@/components/dataSourceManagement/dataSource.vue'),

        // }, {
        //   path: '/business',
        //   name: 'business',
        //   component: () => import(/*webpackChunkName:'admin'*/'@/components/businessModeling/businessModeling.vue'),

        // }]
      }]
    },


  ]
})

export default router
