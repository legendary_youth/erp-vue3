import './assets/main.css'

import { createApp } from 'vue'
import ElementPlus from 'element-plus'

import * as ElIcon from '@element-plus/icons-vue'
import 'element-plus/dist/index.css'

import ECharts from 'vue-echarts'  // 引入ECharts
import "echarts";                  // 全局引入echarts
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
// import * as echarts from 'echarts'
import { createPinia } from 'pinia'
import axios from 'axios'
import VueAxios from 'vue-axios'
import cookies from 'vue3-cookies'

import App from './App.vue'
import router from './router'
// app.config.globalProperties.$echarts = echarts;
const app = createApp(App)

for (let iconName in ElIcon) {
    app.component(iconName, ElIcon[iconName])
}
app.component('ECharts', ECharts)
app.use(createPinia())
app.use(router)
app.use(VueAxios, axios)
app.use(ElementPlus, { locale: zhCn })
app.use(cookies)
// app.use(ElementIcons)

app.mount('#app')
